package Synchronization_Usage_in_SE_WebDriver;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class necessityOfWaits {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "//Users//carleenmahon//Downloads//chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		String[] items = { "Pumpkin", "Brinjal", "Mango", "Strawberry" };
		driver.get("https://rahulshettyacademy.com/seleniumPractise/#/");
		driver.manage().window().maximize();
		Thread.sleep(5000L);
		addItems(driver, items);

	}

	public static void addItems(WebDriver driver, String[] items) {
		int j = 0;
		List<WebElement> produce = driver.findElements(By.cssSelector("h4.product-name"));

		for (int i = 0; i < produce.size(); i++) {

			String[] groceryName = produce.get(i).getText().split("-");
			String formattedName = groceryName[0].trim();

			List itemsNeeded = Arrays.asList(items);

			if (itemsNeeded.contains(formattedName)) {

				j++;
				driver.findElements(By.xpath("//div[@class='product-action']/button")).get(i).click();

				if (j == items.length) {
					break;
				}

			}

		}
		driver.findElement(By.cssSelector("a.cart-icon")).click();
		driver.findElement(By.xpath("//button[contains(text(),'PROCEED TO CHECKOUT')]")).click();
		driver.findElement(By.cssSelector("input.promoCode")).sendKeys("rahulshettyacademy");
		driver.findElement(By.cssSelector("button.promoBtn")).click();
		System.out.println(driver.findElement(By.cssSelector("span.promoInfo")).getText());
	}

}
