package Synchronization_Usage_in_SE_WebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class explicitWaitAssignment {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	
/*		Assignment Instructions:
		 	1. Click the link under the title Demo
		 	2. Print the red text that appears on the WebPage to your console		*/
		
		System.setProperty("webdriver.chrome.driver", "//Users//carleenmahon//Downloads//chromedriver");
		WebDriver driver = new ChromeDriver();
		WebDriverWait wait = new WebDriverWait(driver,5);
		
		driver.get("http://www.itgeared.com/demo/1506-ajax-loading.html");
		driver.manage().window().maximize();
		
		
		driver.findElement(By.xpath("//div[@id='content']/a[2]")).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='results']")));
		System.out.println(driver.findElement(By.xpath("//div[@id='results']")).getText());
	
	}

}
