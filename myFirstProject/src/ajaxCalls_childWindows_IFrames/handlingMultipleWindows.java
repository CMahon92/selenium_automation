package ajaxCalls_childWindows_IFrames;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class handlingMultipleWindows {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.gecko.driver", "//Users//carleenmahon//Downloads//geckodriver");
		
		WebDriver driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	
		driver.get("https://www.google.com/gmail/about/");
		Thread.sleep(2000L);
	
		driver.findElement(By.xpath("//div[@class='h-c-header__bar']/div[4]/ul/li[3]/a")).click();
		System.out.println(driver.getTitle());
	}

}
