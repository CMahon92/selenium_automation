package locatorTechniques;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class locatorTechniques {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

/*		For this automation, I will be using ChromeDriver to launch Netflix. When the browser loads, 
		within the Email address text field, you should see "Lets watch a movie!" and the password
		entered. Once filled out, the browser will navigate to the Netflix link "Need help?" */
			
		
		System.setProperty("webdriver.chrome.driver", "//Users//carleenmahon//Downloads//chromedriver");
		
			WebDriver driver = new ChromeDriver();
				driver.get("https://www.netflix.com/login");
				driver.findElement(By.id("id_userLoginId")).sendKeys("Lets watch a movie!");
				driver.findElement(By.name("password")).sendKeys("123456");
				driver.findElement(By.linkText("Need help?")).click();
				driver.findElement(By.xpath("//*[@id=\'forgot_password_input\']")).sendKeys("Not a valid email");
		
/*		Added to this automation. When the browser reaches the need help screen, "Not a valid email"
		will be entered, "Email Me" button will be clicked. Once done error will be seen on Chrome Browser 
		and printed out in the console. */ 
		
		
				driver.findElement(By.xpath("//*[@id=\'appMountPoint\']/div/div[3]/div/div/button")).click();
		System.out.println(driver.findElement(By.cssSelector("div.input-message.error")).getText());
		
	}

}
