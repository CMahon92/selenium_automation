package locatorTechniques;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class locatorTechnique2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	/*  For this automation I will be using the locator Xpath to launch Hulu through the Firefox WebDriver.
		In order to first access Hulu login page, I used Xpath to click the login button. When the login screen 
		opens up, the email and password field will be filled out. Finally, the log in button will be clicked
		resulting in an error "Error: Your login is invalid. Please try again." Although the automation is able 
		to run successfully, this is an example of Xpath absolute which is considered unreliable and executes the 
		automation slowly. Also, if a developer were to enter a new tag in between the HTML code, the xpath will 
		fail*/
		
		System.setProperty("webdriver.gecko.driver", "//Users//carleenmahon//Downloads//geckodriver");
		
		WebDriver driver = new FirefoxDriver();
	
		driver.get("https://www.hulu.com");
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div/nav/header/div[1]/div[2]/div[2]/button[2]")).click();
		driver.findElement(By.id("email_id")).sendKeys("Choose_a_show_or_movie");
		driver.findElement(By.name("password")).sendKeys("54321");
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div/nav/div/div[2]/div[2]/div/div/div[1]/div/div/button")).click();
	
	}

}
