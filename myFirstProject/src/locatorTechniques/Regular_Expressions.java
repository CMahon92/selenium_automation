package locatorTechniques;

import java.awt.Button;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Regular_Expressions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "//Users//carleenmahon//Downloads//chromedriver");
		
		WebDriver driver = new ChromeDriver();
		
/*		Using regular expressions for xpath and css. This is helpful if an attribute of a name may be too long.
		I used xpath regular expression to type in "rabbit toys" in the text box and css regular expression to 
		click the search Button.class 		*/
		
		driver.get("https://www.petco.com/");
		driver.findElement(By.xpath("//input[contains(@id,'SimpleSearchForm')]")).sendKeys("Rabbit Toys");
		driver.findElement(By.cssSelector("input[class*='search-button']")).click();
	
	}

}
