package locatorTechniques;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class youtube_cssSelector {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

/*		For this automation, I customized a cssSelector. MS_Edge browser will open to YouTube. In the search bar,
		"Javascript for Beginners" will be typed in, next the search button will be clicked. Validated customized 
		cssSelector in MS Edge console						*/
		
		System.setProperty("webdriver.edge.driver", "//Users//carleenmahon//Downloads//msedgedriver");
		
		WebDriver driver = new EdgeDriver();
		
		driver.get("http://youtube.com");
		driver.findElement(By.cssSelector("input[name='search_query']")).sendKeys("Javascript for Beginners");
		driver.findElement(By.cssSelector("[id='search-icon-legacy']")).click();
	}

}
