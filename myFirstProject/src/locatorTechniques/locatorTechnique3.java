package locatorTechniques;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class locatorTechnique3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

/* 		For this automation, I am using the locator cssSelector to input the email and password on Facebook. 
		Once complete, the browser will navigate to the forgot password page. */
		
		System.setProperty("webdriver.chrome.driver", "//Users//carleenmahon//Downloads//chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.get("http://facebook.com");
		
		driver.findElement(By.cssSelector("#email")).sendKeys("Welcome to Facebook");
		driver.findElement(By.cssSelector("#pass")).sendKeys("0246810");
		driver.findElement(By.linkText("Forgot Password?")).click();
	}

}
