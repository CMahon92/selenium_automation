package locatorTechniques;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class facebook_xpath {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "//Users//carleenmahon//Downloads//chromedriver");
		
		//Created my own xpath in order to translate a facebook page in Spanish and type in Bienvenidos in the email text box". 
		//Validated customized xpath in Chrome console.
		
		WebDriver driver = new ChromeDriver();
			driver.get("https://facebook.com");
			driver.findElement(By.xpath("//a[@title='Spanish']")).click();
			driver.findElement(By.xpath("//input[@name='email']")).sendKeys("Bienvenidos");
			
	}

}

