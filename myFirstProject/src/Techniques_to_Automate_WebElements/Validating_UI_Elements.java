package Techniques_to_Automate_WebElements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Validating_UI_Elements {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "//Users//carleenmahon//Downloads//chromedriver");
		WebDriver driver = new ChromeDriver();
		
//		For this automation I will use the method isEnabled() to verify the radio button is selected. Prints true in the console.
		driver.get("https://www.interjet.com/en-us");
		driver.findElement(By.id("btn-one-way")).click();
		System.out.println(driver.findElement(By.id("btn-one-way")).isEnabled());
		
		Thread.sleep(1000L);
		
		driver.findElement(By.id("input-origin")).sendKeys("Los Cabos");
		driver.findElement(By.id("input-destination")).sendKeys("Tijuana");
		
		Thread.sleep(1000L);
		
		driver.findElement(By.cssSelector(".btn-select.btn-passenger")).click();	
		
		Thread.sleep(1000L);
		
		for(int i=1;i<3; i++) {
		driver.findElement(By.xpath("//*[@id=\"ADTPax\"]/div/p[2]/button[2]")).click();
		}	
		
		Thread.sleep(3000L);
		driver.quit();
		
	}

}
