package Techniques_to_Automate_WebElements;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class arrayOfProducts {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

/*		Adding one item to my cart then clicking cart to verify the correct Item was added. */		
		
		System.setProperty("webdriver.chrome.driver", "//Users//carleenmahon//Downloads//chromedriver");
		WebDriver driver = new ChromeDriver();
		
		int j = 0;
		String [] items = {"Pumpkin", "Brinjal", "Mango", "Strawberry"};
		driver.get("https://www.rahulshettyacademy.com/seleniumPractise/#/");
		driver.manage().window().maximize();
		Thread.sleep(5000L);
		
		
		List<WebElement> produce = driver.findElements(By.cssSelector("h4.product-name"));
		
		for (int i=0; i<produce.size();i++) {
			
			String [] groceryName = produce.get(i).getText().split("-");
			String formattedName = groceryName[0].trim();
			
			List itemsNeeded = Arrays.asList(items);
			
			if (itemsNeeded.contains(formattedName)) {
				
				j++;
				driver.findElements(By.xpath("//div[@class='product-action']/button")).get(i).click();
				
				if (j==items.length) {
					break;
				}
				
				
			}
	
	}
		driver.findElement(By.cssSelector("a.cart-icon")).click();

}
}