package Techniques_to_Automate_WebElements;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class parentChild_Relationship_Locators {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "//Users//carleenmahon//Downloads//chromedriver");
		WebDriver driver = new ChromeDriver();
		
		
		driver.get("https://spicejet.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);  
		// Implicit wait is used for page load elements and handle to execute the code
		
		
		/*	For this automation, I will be using a parent-child relationship locator in order to select an option from the 
		dynamic dropdown. This is helpful for locating an object uniquely without having hard code the index. 		*/
		
		
		driver.findElement(By.id("ctl00_mainContent_ddl_originStation1_CTXT")).click();
		driver.findElement(By.xpath("//div[@id='glsctl00_mainContent_ddl_originStation1_CTNR']//a[@value='CMB']")).click(); 
		
		Thread.sleep(2000L);
		
		driver.findElement(By.id("ctl00_mainContent_ddl_destinationStation1_CTXT")).click();
		driver.findElement(By.xpath("//div[@id='glsctl00_mainContent_ddl_destinationStation1_CTNR']//a[@value='DXB']")).click(); 
		
		Thread.sleep(5000L);
		
		driver.quit(); 
		
		
	}

}
