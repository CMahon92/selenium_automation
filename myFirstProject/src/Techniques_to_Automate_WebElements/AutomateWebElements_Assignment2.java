package Techniques_to_Automate_WebElements;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AutomateWebElements_Assignment2 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

/*		Assignment 2 Instructions:
  		1. Select today's date from the dropdown
  		2. Select number Adult Passengers
  		3. Select number Children Passengers
  		4. Enter name of preferred Airline
 		5. Click Search Flights
  		6. Grab Error Message and print it in the console.	*/		
		
		System.setProperty("webdriver.chrome.driver", "//Users//carleenmahon//Downloads//chromedriver");
		WebDriver driver = new ChromeDriver();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		
		driver.get("https://www.cleartrip.com/");
		driver.manage().window().maximize();
		Thread.sleep(5000L);
		
		js.executeScript("window.scrollBy(0,1000)");
		Thread.sleep(2000L);
		
		driver.findElement(By.cssSelector(".span.span15.datePicker")).click();
		Thread.sleep(2000L);
		driver.findElement(By.xpath("//*[contains(@class,'ui-datepicker-days-cell-over')]/a")).click();
		
		Select adults = new Select(driver.findElement(By.cssSelector("[id='Adults']")));
		adults.selectByIndex(2);
		Thread.sleep(2000L);
		Select children = new Select(driver.findElement(By.cssSelector("[id='Childrens']")));
		children.selectByValue("2");
		
		driver.findElement(By.partialLinkText(" Class of travel, Airline preference")).click();
		Thread.sleep(1000L);
		driver.findElement(By.cssSelector("[id='AirlineAutocomplete']")).sendKeys("JetBlue Airways"); 
		driver.findElement(By.xpath("//input[@id='SearchBtn']")).click();
		
		System.out.println(driver.findElement(By.xpath("//div[@id='homeErrorMessage']")).getText());
		
		Thread.sleep(4000L);
		driver.quit();
		
	}

}
