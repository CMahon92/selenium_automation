package Techniques_to_Automate_WebElements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Handeling_Alerts {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "//Users//carleenmahon//Downloads//chromedriver");
		WebDriver driver = new ChromeDriver();
		
/*		For this automation I will enter the name Stormy and click the button alert. The alert window will appear and before
  		the ok button is clicked, it will grab the text from the alert and print it in the console. Once this is complete,
  		the new name that will be entered is Waffles. The confirm button will be clicked and an alert will be displayed. Before
  		clicking the cancel button in the alert, I will grab the text from that alert and have it printed in the console. */
		
		driver.get("https://rahulshettyacademy.com/AutomationPractice/");
		driver.findElement(By.id("name")).sendKeys("Stormy");
		driver.findElement(By.id("alertbtn")).click();
		System.out.println(driver.switchTo().alert().getText());
		Thread.sleep(3000L);
		driver.switchTo().alert().accept();
		
		driver.findElement(By.id("name")).sendKeys("Waffles");
		driver.findElement(By.id("confirmbtn")).click();
		System.out.println(driver.switchTo().alert().getText());
		Thread.sleep(3000L);
		driver.switchTo().alert().dismiss();
		
		Thread.sleep(4000L);
		driver.quit();
	
	}

}
