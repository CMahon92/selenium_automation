package Techniques_to_Automate_WebElements;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class AutoSuggestive_Dropdown {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "//Users//carleenmahon//Downloads//chromedriver");
		WebDriver driver = new ChromeDriver();
		
/* 		For this automation, I will select an option from the auto suggestive drop down.		*/		
		
		driver.get("https://rahulshettyacademy.com/dropdownsPractise/");
		driver.findElement(By.id("autosuggest")).sendKeys("ja");
		
		Thread.sleep(3000L);
		
		
		List<WebElement> options = driver.findElements(By.cssSelector("li[class='ui-menu-item'] a"));
		String place = "";
		for (WebElement option :options) {
			if(option.getText().equalsIgnoreCase("Japan")) {
				place = option.getText();
				option.click();
				break;
				
			}
		}
		
		
		Thread.sleep(4000L);
		System.out.println("In October of 2019, I went to " + place);
		driver.quit();
	
	}

}
