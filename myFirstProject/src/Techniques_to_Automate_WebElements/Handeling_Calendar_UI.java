package Techniques_to_Automate_WebElements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class Handeling_Calendar_UI {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "//Users//carleenmahon//Downloads//chromedriver");
		WebDriver driver = new ChromeDriver();
		
//		Clicks the To destination, inputs "Hawa" for Hawaii, selects Kapalua, HI.		
		driver.get("https://www.delta.com/");
		driver.findElement(By.cssSelector("a[id='toAirportName']")).click();
		driver.findElement(By.xpath("//*[@id=\"search_input\"]")).sendKeys("Hawa");
		Thread.sleep(1000L);
		driver.findElement(By.xpath("//*[@id=\"airport-serach-panel\"]/div/div[2]/div/ul/li[4]/a/span[2]")).click();
		

//		Selects the checkbox "Shop with Miles"		
		Thread.sleep(1000L);
		driver.findElement(By.xpath("//div[contains(@class,'checkbox-wrapper')]/label[@for='shopWithMiles']")).click();
		System.out.println(driver.findElement(By.xpath("//div[contains(@class,'checkbox-wrapper')]/label[@for='shopWithMiles']")).isEnabled());
		Assert.assertTrue((driver.findElement(By.xpath("//div[contains(@class,'checkbox-wrapper')]/label[@for='shopWithMiles']")).isEnabled()));
	
		
//		Selects from dropdown One Way		
		driver.findElement(By.xpath("//span[@class='select-ui-wrapper ']/span[@id='selectTripType-val']")).click();
		driver.findElement(By.xpath("//li[text()='One Way']")).click();
		
		
//		Selects the current date from the calendar then clicks done when the date is selected	
		driver.findElement(By.xpath("//div[contains(@class,'calendarMasterCont')]/div[2]")).click();
		driver.findElement(By.xpath("//td[@class='dl-datepicker-current-day']")).click();
		driver.findElement(By.xpath("//button[@value='done']")).click();
		
		
//		Clicks and selects the total number of passengers and searches for available flights	
		driver.findElement(By.xpath("//span[@id='passengers-val']")).click();
		driver.findElement(By.xpath("//li[@id='ui-list-passengers2']")).click();
		driver.findElement(By.xpath("//button[@id='btn-book-submit']")).click();
		
		Thread.sleep(3000L);
		driver.quit();
		
		

	
	}

}
