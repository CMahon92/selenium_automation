package Techniques_to_Automate_WebElements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;



public class staticDropdown {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.gecko.driver", "//Users//carleenmahon//Downloads//geckodriver");
		WebDriver driver = new FirefoxDriver();
		driver.get("https://www.globalsqa.com/demo-site/select-dropdown-menu/");
		
		
		//1. Automation will open up browser and default to Afghanistan in the drop down. Using selectByValue, the page
		//	 will navigate to the static drop down option "Fiji"
		Select country1 = new Select(driver.findElement(By.xpath("//div[contains(@class,'single_tab_div')]/p/select")));
		country1.selectByValue("FJI");
		
		//2. Using selectByVisibleText, the option "Bermuda" will be selected
		Select country2 = new Select(driver.findElement(By.xpath("//div[contains(@class,'single_tab_div')]/p/select")));
		country2.selectByVisibleText("Bermuda");
		
		//3. Using selectByIndex, the option "Aruba" will be selected. *** Note count starts from 0 ***
		Select country3 = new Select(driver.findElement(By.xpath("//div[contains(@class,'single_tab_div')]/p/select")));
		country3.selectByIndex(12); 
	}

}
