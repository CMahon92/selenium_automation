package Techniques_to_Automate_WebElements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;





public class checkbox_automation {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "//Users//carleenmahon//Downloads//chromedriver");
		WebDriver driver = new ChromeDriver();
		
/* 		For this automation, I will be selecting checkboxes using Selenium. Out of the 6 colors on the checkbox list, 
 		I will only select two of them. The code below shows 3 cssSelector locators with the following values: red, 
 		orange, and purple. Only orange and purple are selected. The method isSelected will check if the checkbox has
 		been selected and return in the console true or false. Next, the method size() is used to display how many 
 		checkboxes are available with a common cssSelector value, in this case, type='checkbox'. The total number of 
 		checkboxes with the cssSelector input[type='checkbox'] will be printed in the console. 			 */		
		
//		In addition to this automation, I added assertions to verify whether the expected result match the actual result. 		
		
		driver.get("https://www.ironspider.ca/forms/checkradio.htm");
		
		System.out.println(driver.getTitle());
		Assert.assertEquals(driver.getTitle(), "Checkboxes & Radio Buttons");
		driver.findElement(By.linkText("checkboxes")).click();
		
		
		System.out.println(driver.findElement(By.cssSelector("input[value='red']")).isSelected());
		Assert.assertFalse(driver.findElement(By.cssSelector("input[value='red']")).isSelected());
		
		driver.findElement(By.cssSelector("input[value='orange']")).click();
		System.out.println(driver.findElement(By.cssSelector("input[value='orange']")).isSelected());
		Assert.assertTrue(driver.findElement(By.cssSelector("input[value='orange']")).isSelected());
		
		driver.findElement(By.cssSelector("input[value='purple']")).click();
		System.out.println(driver.findElement(By.cssSelector("input[value='purple']")).isSelected()); 
		Assert.assertTrue(driver.findElement(By.cssSelector("input[value='purple']")).isSelected());
		
		System.out.println(driver.findElements(By.cssSelector("input[type='checkbox']")).size());
		
		
		Thread.sleep(4000L);
		driver.quit();
		
		
	
	}

}
