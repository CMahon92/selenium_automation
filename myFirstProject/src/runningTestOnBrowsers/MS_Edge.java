package runningTestOnBrowsers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class MS_Edge {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Automating MS Edge Driver and printing title in the console when ran
		
			System.setProperty("webdriver.edge.driver", "//Users//carleenmahon//Downloads//msedgedriver");
			
			WebDriver driver = new EdgeDriver();
			
			driver.get("http://netflix.com");
			System.out.println(driver.getTitle());

//			Below, I have written 3 lines of code where the MS Edge browser will first navigate to
//			Netflix then to Youtube. When the automation is complete, the browser will close.
//			The driver.quit method is similar to driver.close. The key difference is driver.quit
//			closes all browsers opened by selenium script
			 
			driver.get("http://youtube.com");
			driver.navigate().forward();
			driver.quit(); 
	
	}

}
