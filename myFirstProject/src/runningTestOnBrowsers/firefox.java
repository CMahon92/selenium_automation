package runningTestOnBrowsers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class firefox {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Creating a driver for Firefox automation
		//To invoke Firefox browser we need to use gecko driver
	
			System.setProperty("webdriver.gecko.driver", "//Users//carleenmahon//Downloads//geckodriver");
	
			WebDriver driver = new FirefoxDriver();
		
			driver.get("http://facebook.com");
			System.out.println(driver.getTitle());
		
//		Below, I have written 3 lines of code where the Firefox browser will first navigate to FB, 
//		then LinkedIn, then back to FB. When the automation is complete, the browser will close. 
		
			driver.get("http://Linkedin.com");
			driver.navigate().back();
			driver.close();
			
			
			
	}

}
