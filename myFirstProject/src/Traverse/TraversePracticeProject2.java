package Traverse;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TraversePracticeProject2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//1. Traverse between child elements. The text for the  following-sibling::div[2] will print in the console
		//2. Using xpath text(), the automation will click the home page link
		
		System.setProperty("webdriver.gecko.driver", "//Users//carleenmahon//Downloads//geckodriver");
		WebDriver driver = new FirefoxDriver();
		driver.get("http://book.theautomatedtester.co.uk/chapter1");
		
		System.out.println(driver.findElement(By.xpath(".//*[@class='multiplewindow']/following-sibling::div[2]")).getText());
		driver.findElement(By.xpath("//*[text()='Home Page']")).click();
	}

}
