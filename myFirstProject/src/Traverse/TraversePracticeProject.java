package Traverse;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.safari.SafariDriver.WindowType;


public class TraversePracticeProject {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "//Users//carleenmahon//Downloads//chromedriver");
		WebDriver driver = new ChromeDriver();
		
		//1. Page will navigate to stackoverflow, using relative xpath, text will be entered in the search box
		//2. Using absolute xpath, the button will be clicked
		driver.get("https://stackoverflow.com/");
		driver.findElement(By.xpath(".//*[@aria-label='Search']")).sendKeys("The Sign up Button will be clicked next");
		driver.findElement(By.xpath("//div/ol[2]/li[2]/a[2]")).click();
		

		

		

	}

}
