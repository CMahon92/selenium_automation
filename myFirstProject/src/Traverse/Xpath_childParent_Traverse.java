package Traverse;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Xpath_childParent_Traverse {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	
System.setProperty("webdriver.chrome.driver", "//Users//carleenmahon//Downloads//chromedriver");
		WebDriver driver = new ChromeDriver();
		
//		Identifying xpath with Parent Child Traverse Relationship
		
		
		driver.get("http://google.com");
		driver.findElement(By.xpath("//div[contains(@class,'gb_Xa')]/div/div[2]/a")).click();
		driver.findElement(By.xpath("//div[@class='A8SBwf']/div/div/div[2]/input")).sendKeys("Search Image Here");
	

		
	}

}
